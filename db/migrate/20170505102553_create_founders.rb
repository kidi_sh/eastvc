class CreateFounders < ActiveRecord::Migration
  def change
    create_table :founders do |t|
      t.string :name
      t.string :position
      t.string :linkedin
      t.text :description

      t.timestamps null: false
    end
  end
end
