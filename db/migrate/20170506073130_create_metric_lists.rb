class CreateMetricLists < ActiveRecord::Migration
  def change
    create_table :metric_lists do |t|
      t.string :name
      t.string :description
      t.integer :metric_category_id

      t.timestamps null: false
    end
  end
end
