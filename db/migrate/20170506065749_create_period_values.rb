class CreatePeriodValues < ActiveRecord::Migration
  def change
    create_table :period_values do |t|
      t.string :name
      t.integer :period_type_id

      t.timestamps null: false
    end
  end
end
