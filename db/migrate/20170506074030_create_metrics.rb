class CreateMetrics < ActiveRecord::Migration
  def change
    create_table :metrics do |t|
      t.integer :company_id
      t.integer :period_type_id
      t.integer :metric_list_id

      t.timestamps null: false
    end
  end
end
