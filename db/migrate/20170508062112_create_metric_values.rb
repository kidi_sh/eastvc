class CreateMetricValues < ActiveRecord::Migration
  def change
    create_table :metric_values do |t|
      t.integer :metric_id
      t.integer :period_value_id
      t.string :year
      t.string :value

      t.timestamps null: false
    end
  end
end
