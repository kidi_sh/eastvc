class CreateDeals < ActiveRecord::Migration
  def change
    create_table :deals do |t|
      t.integer :company
      t.decimal :capital
      t.string :type
      t.string :round
      t.date :closing
      t.string :admin

      t.timestamps null: false
    end
  end
end
