class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :registration_name
      t.text :headquater
      t.string :country
      t.datetime :founded
      t.string :link
      t.string :business_model
      t.string :industry
      t.string :consumer_target
      t.decimal :latest_financial
      t.decimal :total_fund_raised

      t.timestamps null: false
    end
  end
end
