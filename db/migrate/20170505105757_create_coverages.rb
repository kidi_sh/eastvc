class CreateCoverages < ActiveRecord::Migration
  def change
    create_table :coverages do |t|
      t.string :title
      t.datetime :date
      t.string :link

      t.timestamps null: false
    end
  end
end
