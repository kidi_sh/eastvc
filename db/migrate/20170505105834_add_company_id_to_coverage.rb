class AddCompanyIdToCoverage < ActiveRecord::Migration
  def change
    add_column :coverages, :company_id, :integer
  end
end
