class AddCompanyIdToFounders < ActiveRecord::Migration
  def change
    add_column :founders, :company_id, :integer
  end
end
