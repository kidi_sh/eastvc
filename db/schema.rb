# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170603075136) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.string   "registration_name"
    t.text     "headquater"
    t.string   "country"
    t.datetime "founded"
    t.string   "link"
    t.string   "business_model"
    t.string   "industry"
    t.string   "consumer_target"
    t.decimal  "latest_financial"
    t.decimal  "total_fund_raised"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "coverages", force: :cascade do |t|
    t.string   "title"
    t.datetime "date"
    t.string   "link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "company_id"
  end

  create_table "deals", force: :cascade do |t|
    t.integer  "company"
    t.decimal  "capital"
    t.string   "type"
    t.string   "round"
    t.date     "closing"
    t.string   "admin"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "founders", force: :cascade do |t|
    t.string   "name"
    t.string   "position"
    t.string   "linkedin"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "company_id"
  end

  create_table "metric_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "metric_lists", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "metric_category_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "metric_values", force: :cascade do |t|
    t.integer  "metric_id"
    t.integer  "period_value_id"
    t.string   "year"
    t.string   "value"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "metrics", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "period_type_id"
    t.integer  "metric_list_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "period_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "period_values", force: :cascade do |t|
    t.string   "name"
    t.integer  "period_type_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

end
