require 'test_helper'

class PartsControllerTest < ActionController::TestCase
  test "should get navigation" do
    get :navigation
    assert_response :success
  end

  test "should get footer" do
    get :footer
    assert_response :success
  end

end
