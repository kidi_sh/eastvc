class Company < ActiveRecord::Base
    has_many :founders, :dependent => :destroy
    accepts_nested_attributes_for :founders,  :reject_if => :all_blank, :allow_destroy => true
    has_many :coverages, :dependent => :destroy
    accepts_nested_attributes_for :coverages,  :reject_if => :all_blank, :allow_destroy => true
    has_many :metrics
    has_many :metric_lists
end
