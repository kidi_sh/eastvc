class MetricList < ActiveRecord::Base
    belongs_to :metric_category
    has_many :metric
end
