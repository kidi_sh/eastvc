class MetricValue < ActiveRecord::Base
    belongs_to :metric 
    belongs_to :period_value
end
