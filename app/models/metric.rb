class Metric < ActiveRecord::Base
    belongs_to :company
    
    belongs_to :metric_list
    belongs_to :period_type
    
    has_many :metric_values, :dependent => :destroy
    accepts_nested_attributes_for :metric_values,  :reject_if => :all_blank, :allow_destroy => true
end
