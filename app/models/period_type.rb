class PeriodType < ActiveRecord::Base
    has_many :period_values
    has_many :metrics
end