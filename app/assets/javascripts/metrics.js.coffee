# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
jQuery ->
    x = document.getElementById('mtable').rows.length
    $('#metric_period_type_id').change ->
        period_type = $('#metric_period_type_id :selected').text()
        options = $(period_value).filter("optgroup[label='#{period_type}']").html()
        if options
            for i in [0..x]
                $('#metric_metric_values_attributes_'+i+'_period_value_id').html(options)
        else
            $('#metric_metric_values_attributes_'+i+'_period_value_id')
