class MetricsController < ApplicationController
  before_action :set_metric, only: [:show, :edit, :update, :destroy]
  layout "dashboard"

  # GET /metrics
  # GET /metrics.json
  def index
    @metrics = Metric.all
  end

  # GET /metrics/1
  # GET /metrics/1.json
  def show
  end

  # GET /metrics/new
  def new
    @company = Company.find(params[:company_id])
    @metric = @company.metrics.build
    @metric_value = @metric.metric_values.build 
  end

  # GET /metrics/1/edit
  def edit
    @company = Company.find(params[:company_id])
    @metric = @company.metrics.find(params[:id])
  end

  # POST /metrics
  # POST /metrics.json
  def create
    @metric = Metric.new(metric_params)
    respond_to do |format|
      if @metric.save
        format.html { redirect_to company_metric_path(@metric.company_id ,@metric, :ml_id => @metric.metric_list_id), notice: 'Metric was successfully created.' }
        format.json { render :show, status: :created, location: [@company,@metric] }
      else
        format.html { render :new }
        format.json { render json: @metric.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /metrics/1
  # PATCH/PUT /metrics/1.json
  def update
    respond_to do |format|
      if @metric.update(metric_params)
        format.html { redirect_to company_metric_path(@metric.company_id ,@metric, :ml_id => @metric.metric_list_id), notice: 'Metric was successfully updated.' }
        format.json { render :show, status: :ok, location: @metric }
      else
        format.html { render :edit }
        format.json { render json: @metric.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /metrics/1
  # DELETE /metrics/1.json
  def destroy
    @metric.destroy
    respond_to do |format|
      format.html { redirect_to metrics_url, notice: 'Metric was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_metric
      @metric = Metric.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def metric_params
      params.require(:metric).permit(:company_id, :period_type_id, :metric_list_id, metric_values_attributes: [:id, :period_value_id, :year,:value,:_destroy])
    end
end
