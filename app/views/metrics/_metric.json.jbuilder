json.extract! metric, :id, :company_id, :period_type_id, :metric_list_id, :created_at, :updated_at
json.url metric_url(metric, format: :json)