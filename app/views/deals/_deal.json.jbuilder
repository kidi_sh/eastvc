json.extract! deal, :id, :company, :capital, :type, :round, :closing, :admin, :created_at, :updated_at
json.url deal_url(deal, format: :json)